import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { ShopComponent } from './shop/shop.component';
import { OffersComponent } from './offers/offers.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [CatalogComponent, ShopComponent, OffersComponent],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    SharedModule
  ]
})
export class CatalogModule { }
