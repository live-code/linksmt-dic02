import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lmt-catalog',
  template: `
   <button routerLink="shop">shop</button>
   <button routerLink="offers">offers</button>
   <button routerLink="lastminute">last minute</button>
   
   <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
