import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogComponent } from './catalog.component';
import { ShopComponent } from './shop/shop.component';
import { OffersComponent } from './offers/offers.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogComponent,
    children: [
      { path: 'offers', component: OffersComponent },
      { path: 'shop', component: ShopComponent },
      { path: 'lastminute', loadChildren: () => import('./lastminute/lastminute.module').then(m => m.LastminuteModule) }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
