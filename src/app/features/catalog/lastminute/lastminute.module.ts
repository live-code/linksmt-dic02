import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LastminuteRoutingModule } from './lastminute-routing.module';
import { LastminuteComponent } from './lastminute.component';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [LastminuteComponent],
  imports: [
    CommonModule,
    LastminuteRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class LastminuteModule { }
