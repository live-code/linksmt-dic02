import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lmt-lastminute',
  template: `
    <p>
      lastminute works!
    </p>
    <input type="text" [ngModel]>
    <lmt-card></lmt-card>
  `,
  styles: [
  ]
})
export class LastminuteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
