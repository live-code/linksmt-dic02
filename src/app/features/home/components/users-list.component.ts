import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'lmt-users-list',
  template: `
    <li *ngFor="let user of users">
      {{user.email}}
      <button (click)="deleteEmail.emit(user.id)">delete</button>
    </li>
  `,
  styles: [
  ]
})
export class UsersListComponent {
  @Input() users: any[];
  @Output() deleteEmail: EventEmitter<number> = new EventEmitter<number>();
}
