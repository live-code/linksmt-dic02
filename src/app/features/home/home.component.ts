import { Component } from '@angular/core';
import { HomeService } from './services/home.service';

@Component({
  selector: 'lmt-home',
  template: `
    <h1>Home!</h1>
    <router-outlet></router-outlet>
    <lmt-users-list [users]="emails" (deleteEmail)="deleteHandler($event)"></lmt-users-list>
    <div>{{emails?.length}} users</div>
  `,
})
export class HomeComponent {
  emails: any[];

  constructor(private homeService: HomeService) {
    homeService.getUsers()
      .subscribe(res => this.emails = res);
  }

  deleteHandler(email): void {
    console.log('delete', email);
  }

}
