import { Injectable } from '@angular/core';
import { filter, map, switchMap, toArray } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../../../core/auth/auth.service';

@Injectable({ providedIn: 'root' })
export class HomeService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  getUsers(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:3000/users')
      .pipe(
        map(users => users.map(u => {
          return {
            email: u.email,
            id: u.id
          };
        })),
      );
  }
}
