import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'lmt-login',
  template: `<!--

      <lmt-sign-in *ngIf="section === 'login'"></lmt-sign-in>
      <lmt-lost-password *ngIf="section === 'lostpass'"></lmt-lost-password>
      <lmt-registration  *ngIf="section === 'registration'"></lmt-registration>
      <br>
      <button (click)="section = 'login'" *ngIf="section !== 'login'">login</button>
      <button (click)="section = 'lostpass'">LostPass</button>
      <button (click)="section = 'registration'">Registration</button>
      -->
  
      <h1>Login page</h1>
      <div style="border: 1px solid black; padding: 10px">
      
        <router-outlet></router-outlet>
    
        <a routerLink="signin" routerLinkActive="active">sigin</a> - 
        <a routerLink="registration" routerLinkActive="active">Registration</a> - 
        <a routerLink="lost" routerLinkActive="active">Lost pass</a>

      </div>
  `,
  styles: [`
    .active {
      color: deeppink;
    }
  `]
})
export class LoginComponent implements OnInit {
  section = 'login';

  ngOnInit(): void {
  }

}
