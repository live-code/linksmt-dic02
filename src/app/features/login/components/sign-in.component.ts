import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../core/auth/auth.service';
import { Credentials } from '../../../core/auth/credentials';

@Component({
  selector: 'lmt-sign-in',
  template: `
    
    <h1>SIGN IN</h1>
    <form #f="ngForm" (submit)="loginHandler(f)">
      <input type="text" placeholder="username" [ngModel] name="username" required>
      <input type="password" placeholder="password" [ngModel] name="password" required>
      <button type="submit" [disabled]="f.invalid">LOGIN</button>
    </form>
    
    <br>
  `,
  styles: [
  ]
})
export class SignInComponent {
  constructor(private authService: AuthService) { }

  loginHandler(form: NgForm): void {
    const obj: Credentials = {
      username: form.value.username,
      password: form.value.password
    };
    this.authService.signIn(obj)
  }
}
