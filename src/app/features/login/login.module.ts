import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';
import { SignInComponent } from './components/sign-in.component';
import { RegistrationComponent } from './components/registration.component';
import { LostPasswordComponent } from './components/lost-password.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    LoginComponent,
    SignInComponent,
    RegistrationComponent,
    LostPasswordComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'registration', component: RegistrationComponent },
          { path: 'lost', component: LostPasswordComponent },
          { path: 'signin', component: SignInComponent },
          { path: '', redirectTo: 'signin'}
        ]
      }

    ])
  ]
})
export class LoginModule { }
