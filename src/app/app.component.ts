import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { filter, first, map } from 'rxjs/operators';

@Component({
  selector: 'lmt-root',
  template: `

    <lmt-navbar></lmt-navbar>

    <router-outlet></router-outlet>
    
    <hr>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div>footer</div>
    
    <div *ngIf="loading">loading..</div> 
    
    
  `,
})
export class AppComponent {
  showNavbar: boolean;
  loading = false;

  constructor(private router: Router, private activateRoute: ActivatedRoute) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.loading = true;
      }else if(event instanceof NavigationEnd) {
        this.loading = false;
      }
    });

    /*router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map((event: NavigationEnd) => event.url),
      )
      .subscribe(url => this.showNavbar = url !== '/login')*/
  }
}
