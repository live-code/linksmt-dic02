import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lmt-card',
  template: `
    <div class="card">
      <div>Title</div>
      <div>card works!</div>
      <lmt-card-info></lmt-card-info>
    </div>
  `,
  styles: [`
    .card {
      border: 1px solid #222;
      border-radius: 10px;
    }
  `]
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
