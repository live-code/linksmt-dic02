import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { CardInfoComponent } from './components/card-info.component';
import { MapComponent } from './components/map/map.component';

@NgModule({
  declarations: [
    CardComponent,
    CardInfoComponent,
    MapComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    MapComponent,
  ]
})
export class SharedModule { }
