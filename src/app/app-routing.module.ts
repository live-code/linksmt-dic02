import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, Route, PreloadingStrategy, ActivatedRoute } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';
import { Observable, of } from 'rxjs';

export class AppCustomPreloader implements PreloadingStrategy {
  preload(route: Route, load: () => any): Observable<any> {
    return route.data?.preload ? load() : of(null);
  }
}

const routes: Routes = [
  { path: 'login',
    loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule),
  },
  { path: 'home',
    loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'admin',
    loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule),
    canActivate: [AuthGuard],
    data: { preload: true, xyz: 123 }

  },
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: '**', redirectTo: 'login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: AppCustomPreloader })],
  exports: [RouterModule],
  providers: [AppCustomPreloader]
})
export class AppRoutingModule { }

