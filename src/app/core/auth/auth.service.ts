import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Credentials } from './credentials';
import { Auth } from './auth';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthService {
  auth: Auth;

  constructor(private http: HttpClient, private router: Router) {
  }

  // signIn({ username, password}): void {
  signIn(credentials: Credentials): void {
    const params = new HttpParams()
      .set('username', credentials.username)
      .set('password', credentials.password);

    this.http.get<Auth>(`http://localhost:3000/login`, { params })
      .subscribe(result => {
        this.auth = result;
        this.router.navigateByUrl('home');
        localStorage.setItem('auth', JSON.stringify(result))
      });
  }

  signOut(): void {
    this.auth = null;
    localStorage.removeItem('auth');
    this.router.navigateByUrl('login')
  }

  isLogged(): boolean {
    // return !!this.auth;
    return !!localStorage.getItem('auth');
  }

  getToken(): string {
    const auth: Auth = JSON.parse(localStorage.getItem('auth')) as Auth;
    return auth.token;
  }
}
