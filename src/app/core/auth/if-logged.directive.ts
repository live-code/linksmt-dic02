import { Directive, HostBinding, Input } from '@angular/core';
import { AuthService } from './auth.service';

@Directive({
  selector: '[lmtIfLogged]'
})
export class IfLoggedDirective {
  @Input() lmtIfLogged: 'admin' | 'client';

  @HostBinding('style.display') get display(): string {
    return this.authService.auth?.role === this.lmtIfLogged
      ? null : 'none';
  }

  constructor(private authService: AuthService) {
    console.log('if logged')
  }

}
