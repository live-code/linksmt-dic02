import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let clonedReq = req;
    // if (req.url.includes('users')) {
    if (this.authService.isLogged()) {
      clonedReq = req.clone({
        // setHeaders: { Authorization: this.authService.auth.token}
        setHeaders: { Authorization: this.authService.getToken()}
      });
    }
    return next.handle(clonedReq)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 0:
              case 401:
                this.router.navigateByUrl('login')
                this.authService.signOut();
                break;

              case 404:
                // show message
                break;
            }
          }
          // keep stream alive
          return of(null)
        })
      )
  }

}


// ----------o-------oxo----o-----o--->
