import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './layouts/navbar.component';
import { RouterModule } from '@angular/router';
import { IfLoggedDirective } from './auth/if-logged.directive';


@NgModule({
  declarations: [
    NavbarComponent,
    IfLoggedDirective,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [NavbarComponent]
})
export class CoreModule { }
