import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'lmt-navbar',
  template: `
    <button routerLink="login" routerLinkActive="bg">Login</button>
    <button [routerLink]="'home'"  routerLinkActive="bg">Home</button>
    <button routerLink="admin" routerLinkActive="bg" lmtIfLogged="admin">admin</button>
    <button [routerLink]="'catalog'"  routerLinkActive="bg" lmtIfLogged="client">catalog</button>
    <button (click)="authService.signOut()">Logout</button>
    <span>{{authService.auth?.displayName}}</span>
    <hr>

  `,
  styles: [`
    .bg { background-color: deeppink}
  `]
})
export class NavbarComponent implements OnInit {
  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

}
